let i = 0;
let point = 0;

let tbImgPropose = [
    "/image-sans/chien.png",
    "/image-sans/neko.png",
    "/image-sans/grenouille.png",
    "/image-sans/souris.png",
    "/image-sans/tigre.png",
    "/image-sans/vache.png",
    "/image-sans/singe.png",
    "/image-sans/cochon.png",
    "/image-sans/coque.png",
    "/image-sans/renard.png",
];

let tbReponse1: string[] = ["Nyao", "Nyao", "Kelo Kelo", "Dhuuuu","Gaooo", "mooooh","U ki ki","Buu Buu", "Ku ku ku ku", "Kon Kon"];
let tbReponse2: string[] = ["Wan Wan", "Gaao", "Gua Gua", "Chuuuu","Waffff","muuuuh","Uho uho", "Oink Oink", "Kokekkokko", "Kyuu Kyuu"];
let correctReponse: string[] = ["Wan Wan", "Nyao", "Kelo Kelo", "Chuuuu","Gaooo","muuuu" ,"U ki ki","Buu Buu", "Kokekkokko", "Kon Kon"];

let tbReponse1AD: string[] = ["Neko", "Nezumi", "Kaelu", "Nezumi","Tora","Ushi","Saru", "Inoshishi", "Tori", "Uma"];
let tbReponse2AD: string[] = ["Inu", "Neko", "Inu", "Usagi","Taiga-", "Uma","Sake","Buta", "Niwatori", "Kitsune"];
let correctReponseAD: string[] = ["Inu", "Neko", "Kaeru", "Nezumi","Tora", "Buta", "Niwatori", "Kitsune"];

let indiceChoix1: string[] = [
    "Le cris de Chien est Wan Wan ",
    "Le cris de Neko est Nyao ",
    "Le cris de Kaeru est Kelo Kelo ",
    "Le cris de Nezumi est Chuuu ",
    "Le cris de Tigre est Gaooo ",
    "Le cris de Vache est mooooh ",
    "Le cris de Singe est U ki ki ",
    "Le cris de Buta est Buu Buu ",
    "Le cris de Coque est Kokkekkokko",
    "Le cris de Kitsune est Kon kon ",
]



const blocChoix = document.querySelector<HTMLImageElement>("#bloc-choix");
const image = document.querySelector<HTMLImageElement>("#imgQuestion");
const blocImg = document.querySelector<HTMLImageElement>("#blocImage");
const reponse1 = document.querySelector<HTMLElement>("#reponse1");
const reponse2 = document.querySelector<HTMLElement>("#reponse2");
const choix1 = document.querySelector<HTMLElement>("#choix1");
const choix2 = document.querySelector<HTMLElement>("#choix2");
const score = document.querySelector<HTMLElement>("span");
const afficherFin = document.querySelector<HTMLElement>("#afficherFin");
const btnIndice = document.querySelector<HTMLElement>("#indice");
const blocIndice = document.querySelector<HTMLElement>("#bloc-Indice");
const blocReponse = document.querySelector<HTMLElement>("#bloc-reponse");
const continuer = document.querySelector<HTMLElement>("#continuer");
const blocCorrect = document.querySelector<HTMLElement>("#bloc-correct");
const blocFaux = document.querySelector<HTMLElement>("#bloc-faux");
const phraseDebut = document.querySelector<HTMLElement>("#phraseDebut");
const bravo = document.querySelector<HTMLElement>("#bravo");
const encore = document.querySelector<HTMLElement>("#encore");





//////////Choix de jeu //////////////////////////////4



choix1.addEventListener("click", () => {

  //  phraseDebut.style.display = "none";
    blocChoix.style.display ="none";
    blocReponse.style.display = "flex";

    AfficherJeu(tbReponse1, tbReponse2);

    reponse1.addEventListener('click', () => {
        verifier(reponse1, correctReponse, tbReponse1, tbReponse2);
        console.log("i :", i);
    })
    reponse2.addEventListener('click', () => {
        verifier(reponse2, correctReponse, tbReponse1, tbReponse2);
        console.log("i :", i);
    })
})


choix2.addEventListener("click", () => {

  //  phraseDebut.style.display = "none";
    blocChoix.style.display ="none";
    blocReponse.style.display = "flex";

    AfficherJeu(tbReponse1AD, tbReponse2AD);


    reponse1.addEventListener('click', () => {
        verifier(reponse1, correctReponseAD, tbReponse1AD, tbReponse2AD);
    })
    reponse2.addEventListener('click', () => {
        verifier(reponse2, correctReponseAD, tbReponse1AD, tbReponse2AD);
    })
})


///////////////////////////////////////////////////////////
function AfficherJeu(TB1: string[], TB2: string[]) {

    //afficher 1er question
    image.src = tbImgPropose[0];
    reponse1.textContent = TB1[0];
    reponse2.textContent = TB2[0];
    blocIndice.textContent = indiceChoix1[0];
}


////////  FUNCTION  Verfier    /////////////////////////

function verifier(Button: HTMLElement, CORRECT: string[], TB1: string[], TB2: string[]) {

    if (Button.textContent == CORRECT[i]) {
        console.log("correct");
        console.log(i);
        console.log(point);

        show3s(blocCorrect, "flex", 1000);
        point++
        i++

    } else {
        console.log("false");
        console.log(i);
        console.log(point);

        show3s(blocFaux, "flex", 1000)
        i++
    }

    

// Afficher prochaines questions, button et score

    if (i < TB1.length) {
        image.src = tbImgPropose[i];
        reponse1.textContent = TB1[i];
        reponse2.textContent = TB2[i];
        blocIndice.textContent = indiceChoix1[i];
        score.textContent = point+"";

    } else {
        console.log("fin");
        score.textContent = point +"";
        afficherFin.style.display = "flex";
        blocImg.style.display = "none";

        continuer.addEventListener("click", () => {
            // console.log("continuer")
            // afficherFin.style.display = "none";
            // i = 0;
            // point = 0;
            location.reload()
        })
    }
}


//////   Indice    //////////////////////


btnIndice.addEventListener("click", () => {

    show3s(blocIndice, "flex", 2000);

})

/////////////FUNCTION show3s/////////////

function show3s(BLOC: HTMLElement, DISPLAY: string, S: number) {

    BLOC.style.display = DISPLAY;

    setTimeout(() => {

        BLOC.style.display = "none";

    }, S);
}


